<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')
    ->group(function () {

        Auth::routes();

        Route::namespace('Admin')
            ->middleware(['auth'])
            ->name('admin.')
            ->group(function () {

                // Route Home
                Route::get('home', 'HomeController@index')->name('index');

                // Route Designer
                Route::resource('designer', 'DesignerController');

                // Route Email
                Route::resource('email', 'EmailController');
                Route::get('email/invite/{email}', 'EmailController@invite')->name('email.invite');

                // Route Link
                Route::get('links', 'LinkController@index')->name('link.index');

                // Route User Register
                Route::get('user_register', 'UserRegisterController@index')->name('userRegister.index');
            });
    });

// Register
Route::get('designers/find', 'DesignerController@find')->name('designer.find');
Route::get('register/{url}', 'RegisterInviteController@register')->name('register.invite');
Route::post('register/store/{link}', 'RegisterInviteController@store')->name('register.invite.store');

// Home
Route::get('', 'HomeController@index')->name('index');
