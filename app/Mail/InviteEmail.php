<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InviteEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = url('/') . '/register/' . $this->link->url;
        $this->to($this->link->email->email);
        $this->subject("Invited to Event HUNTBAZAAR");
        $this->from("admin@hunterstreet.com", "Admin Hunterstreet");

        return $this->view('emails.invite', compact('url'));
    }
}
