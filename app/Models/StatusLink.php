<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusLink extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    
    const STATUS_PENDING = 'pending';
    const STATUS_OPEN    = 'open';
    const STATUS_FILL    = 'fill';

    public $timestamps = true;
    protected $fillable = ['status'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function getStatusId($statusTypeKey)
    {
        $status = self::where('status', 'like', "%" . $statusTypeKey . "%")->first();

        return $status->id;   
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getStatusLabelAttribute()
    {
        if ($this->status == self::STATUS_PENDING) {
            return "<label class='badge badge-warning'>" . $this->status . "</label>";
        }

        if ($this->status == self::STATUS_OPEN) {
            return "<label class='badge badge-primary'>" . $this->status . "</label>";
        }
        
        if ($this->status == self::STATUS_FILL) {
            return "<label class='badge badge-success'>" . $this->status . "</label>";
        }
    }
    
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
