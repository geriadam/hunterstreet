<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRegister extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    const GENDER_MALE = 'm';
    const GENDER_FEMALE = 'f';

    public $timestamps = true;
    protected $fillable = ['link_id', 'email', 'name', 'birth_date', 'gender'];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function link()
    {
        return $this->belongsTo(Link::class);
    }

    public function designers()
    {
        return $this->belongsToMany(Designer::class, 'user_register_designers', 'user_register_id', 'designer_id');
    }
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    
    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getGenderLabelAttribute()
    {
        if ($this->gender == self::GENDER_MALE) {
            return "<label class='badge badge-info'>male</label>";
        }
        
        if ($this->gender == self::GENDER_FEMALE) {
            return "<label class='badge badge-danger'>female</label>";
        }
    }

    public function getDesignersLabelAttribute()
    {
        $result = "";
        if ($this->designers()) {
            foreach ($this->designers()->get() as $designer) {
                $result .= "<label class='badge badge-success'>" . $designer->name . "</label> ";
            }
        }

        return $result;
    }
    
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
