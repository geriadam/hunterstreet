<?php

namespace App\Helpers;

use Auth;
use Ekko;
use Request;

/**
 * Class Menu
 *
 * @package App\Helpers
 */
class Menu
{
    /**
     * @return array
     */
    public static function getArrOfMenu()
    {
        return [
            [
                'label'   => 'Dashboard',
                'url'     => 'admin.index',
                'icon'    => 'dashboard',
                'active'  => Ekko::isActiveRoute('admin.index'),
                'visible' => true,
                'header'  => false,
            ],
            [
                'label'   => 'Designer',
                'url'     => 'admin.designer.index',
                'icon'    => 'users',
                'active'  => Ekko::isActiveRoute('admin.designer.*'),
                'visible' => true,
                'header'  => false,
            ],
            [
                'label'   => 'Email',
                'url'     => 'admin.email.index',
                'icon'    => 'envelope-o',
                'active'  => Ekko::isActiveRoute('admin.email.*'),
                'visible' => true,
                'header'  => false,
            ],
            [
                'label'   => 'Link',
                'url'     => 'admin.link.index',
                'icon'    => 'link',
                'active'  => Ekko::isActiveRoute('admin.link.*'),
                'visible' => true,
                'header'  => false,
            ],
            [
                'label'   => 'User Register',
                'url'     => 'admin.userRegister.index',
                'icon'    => 'users',
                'active'  => Ekko::isActiveRoute('admin.userRegister.*'),
                'visible' => true,
                'header'  => false,
            ],
        ];
    }
}
