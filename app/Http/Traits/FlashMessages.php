<?php

namespace App\Http\Traits;

trait FlashMessages
{
    public function flash($key, $message)
    {
        session()->flash($key, $message);
    }

    /*
    |--------------------------------------------------------------------------
    | FLASH SUCCESS
    |--------------------------------------------------------------------------
    */

    public function flashSuccessStore($message = 'Data successfull Created')
    {
        $this->flash('success', $message);
    }

    public function flashSuccessUpdate($message = 'Data successfull Updated')
    {
        $this->flash('info', $message);
    }

    public function flashSuccessDelete($message = 'Data successfull Deleted')
    {
        $this->flash('warning', $message);
    }

    /*
    |--------------------------------------------------------------------------
    | FLASH FAILED
    |--------------------------------------------------------------------------
    */

    public function flashFailedStore($exception = null, $message = 'Oops, Data not saved successfully with error ')
    {
        $this->flash('danger', $message . $exception);
    }

    public function flashFailedUpdate($exception = null, $message = 'Oops, Data not updating successfully with error ')
    {
        $this->flash('danger', $message . $exception);
    }

    public function flashFailedDelete($exception = null, $message = 'Oops, Data was not deleted successfully with an error ')
    {
        $this->flash('danger', $message . $exception);
    }
}
