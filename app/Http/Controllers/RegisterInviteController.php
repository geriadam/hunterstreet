<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRegisterRequest;
use App\Http\Traits\FlashMessages;
use App\Jobs\SendMail;
use App\Models\Link;
use App\Models\StatusLink;
use App\Models\UserRegister;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class RegisterInviteController extends Controller
{
    use FlashMessages;

    public function register($url)
    {
        $link = Link::where('url', $url)->first();
        $date = Carbon::create(2021, 8, 10, 14, 30, 00);
        $startDate = Carbon::parse($date)->isoFormat('MMM D, Y HH:mm:ss');
        $endDate = Carbon::parse($date->addMonth())->isoFormat('MMM D, Y HH:mm:ss');

        if (!$link) {
            abort(404);
        }

        if (Carbon::now()->format('Y-m-d H:i:s') >= Carbon::parse($endDate)->format('Y-m-d H:i:s')) {
            return view('expired');
        }

        if ($link->status_id == StatusLink::getStatusId(StatusLink::STATUS_PENDING)) {
            $link->update(["status_id" => StatusLink::getStatusId(StatusLink::STATUS_OPEN)]);
        }

        return view('register', compact('link', 'startDate', 'endDate'));
    }

    public function store(UserRegisterRequest $request, Link $link)
    {
        DB::beginTransaction();
        try {
            $request->request->add([
                'link_id' => $link->id,
                'email' => $link->email->email,
                'birth_date' => Carbon::parse($request->birth_date)->format('Y-m-d'),
            ]);
            $userRegister = UserRegister::create($request->all());
            $userRegister->designers()->attach($request->designers);

            // Updated Link
            $link->update([
                'code' => \Str::random(6),
                'status_id' => StatusLink::getStatusId(StatusLink::STATUS_FILL),
            ]);

            // Send Email
            $emailJob = (new SendMail($link->email->email))->delay(Carbon::now()->addMinutes(60));
            dispatch($emailJob);

            DB::commit();
            $this->flashSuccessStore("Successful register this event");
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollback();
            $this->flashSuccessDelete("Failed register this event");
            return redirect()->back();
        }
    }
}
