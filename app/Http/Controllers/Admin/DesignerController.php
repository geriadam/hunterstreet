<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\FlashMessages;
use App\Http\Requests\DesignerRequest;
use App\Models\Designer;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class DesignerController
 *
 * @package App\Http\Controllers\Admin
 */
class DesignerController extends Controller
{
    use FlashMessages;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Designer::query();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row) {
                    $editUrl = route('admin.designer.edit', $row->id);
                    $deleteUrl = route('admin.designer.destroy', $row->id);
                    return view('admin.components.action', compact('editUrl', 'deleteUrl', 'row'));
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        
        return view('admin.pages.designer.index');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.designer.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\DesignerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DesignerRequest $request)
    {
        $designer = Designer::create($request->all());
        if ($designer) {
            $this->flashSuccessStore();
            return redirect()->route('admin.designer.index');
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Designer $designer
     * @return \Illuminate\Http\Response
     */
    public function edit(Designer $designer)
    {
        return view('admin.pages.designer.edit', compact('designer'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\DesignerRequest $request
     * @param \App\Models\Designer $designer
     * @return \Illuminate\Http\Response
     */
    public function update(DesignerRequest $request, Designer $designer)
    {
        $designer = $designer->update($request->all());
        if ($designer) {
            $this->flashSuccessUpdate();
            return redirect()->route('admin.designer.index');
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Designer $designer
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Designer $designer)
    {
        if ($designer->delete()) {
            $this->flashSuccessDelete();
            return redirect()->route('admin.designer.index');
        }
    }
}
