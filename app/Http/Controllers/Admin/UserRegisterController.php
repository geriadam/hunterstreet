<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserRegister;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class UserRegisterController
 *
 * @package App\Http\Controllers\Admin
 */
class UserRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = UserRegister::query();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('gender_label', function($row) {
                    return $row->gender_label;
                })
                ->addColumn('designers_label', function($row) {
                    return $row->designers_label;
                })
                ->rawColumns(['gender_label', 'designers_label'])
                ->make(true);
        }
        
        return view('admin.pages.userRegister.index');
    }
}
