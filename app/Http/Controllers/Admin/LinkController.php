<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Link;
use DataTables;
use Illuminate\Http\Request;

/**
 * Class LinkController
 *
 * @package App\Http\Controllers\Admin
 */
class LinkController extends Controller
{    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Link::query();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function($row) {
                    return $row->status->status_label;
                })
                ->addColumn('email', function($row) {
                    return $row->email->email;
                })
                ->rawColumns(['status'])
                ->make(true);
        }
        
        return view('admin.pages.link.index');
    }
}
