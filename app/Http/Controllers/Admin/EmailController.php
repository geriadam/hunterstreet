<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmailRequest;
use App\Http\Traits\FlashMessages;
use App\Mail\InviteEmail;
use App\Models\Email;
use App\Models\Link;
use App\Models\StatusLink;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Mail;

/**
 * Class EmailController
 *
 * @package App\Http\Controllers\Admin
 */
class EmailController extends Controller
{
    use FlashMessages;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Email::query();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row) {
                    $inviteUrl = route('admin.email.invite', $row->id);
                    $editUrl = route('admin.email.edit', $row->id);
                    $deleteUrl = route('admin.email.destroy', $row->id);
                    return view('admin.components.action_email', compact('inviteUrl', 'editUrl', 'deleteUrl', 'row'));
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        
        return view('admin.pages.email.index');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.email.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\EmailRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmailRequest $request)
    {
        $email = Email::create($request->all());
        if ($email) {
            $this->flashSuccessStore();
            return redirect()->route('admin.email.index');
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Email $email
     * @return \Illuminate\Http\Response
     */
    public function edit(Email $email)
    {
        return view('admin.pages.email.edit', compact('email'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\EmailRequest $request
     * @param \App\Models\Email $email
     * @return \Illuminate\Http\Response
     */
    public function update(EmailRequest $request, Email $email)
    {
        $email = $email->update($request->all());
        if ($email) {
            $this->flashSuccessUpdate();
            return redirect()->route('admin.email.index');
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Email $email
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Email $email)
    {
        if ($email->delete()) {
            $this->flashSuccessDelete();
            return redirect()->route('admin.email.index');
        }
    }

    public function invite(Email $email)
    {
        DB::beginTransaction();
        try {
            $statusId = StatusLink::getStatusId(StatusLink::STATUS_PENDING);
            $url = \Str::random(10);
            $link = Link::create([
                'status_id' => $statusId,
                'email_id' => $email->id,
                'url' => $url,
            ]);
            Mail::send(new InviteEmail($link));
            DB::commit();
            $this->flashSuccessStore("Successfull Invite " . $email->email);
            return redirect()->back();
        } catch (Exception $e) {
            DB::rollback();
            $this->flashSuccessDelete("Failed Invite " . $email->email);
            return redirect()->back();
        }
    }
}
