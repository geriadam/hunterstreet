<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Link;
use App\Models\StatusLink;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers\Admin
 */
class HomeController extends Controller
{
    public function index()
    {
        $statusPending = Link::whereHas('status', function($query) {
            $query->where('status', StatusLink::STATUS_PENDING);
        })->count();

        $statusOpen = Link::whereHas('status', function($query) {
            $query->where('status', StatusLink::STATUS_OPEN);
        })->count();

        $statusFill = Link::whereHas('status', function($query) {
            $query->where('status', StatusLink::STATUS_FILL);
        })->count();

        return view('admin.pages.index', compact('statusPending', 'statusOpen', 'statusFill'));
    }
}
