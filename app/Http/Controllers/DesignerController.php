<?php

namespace App\Http\Controllers;

use App\Models\Designer;
use Illuminate\Http\Request;

class DesignerController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function find(Request $request)
    {
        $term = trim($request->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $designers = Designer::where('name', 'like', '%' . $term . '%')->limit(5)->get();

        $formatted_designers = [];

        foreach ($designers as $designer) {
            $formatted_designers[] = ['id' => $designer->id, 'text' => $designer->name];
        }

        return \Response::json($formatted_designers);
    }
}
