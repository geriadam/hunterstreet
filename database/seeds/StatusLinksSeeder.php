<?php

use App\Models\StatusLink;
use Illuminate\Database\Seeder;

class StatusLinksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusLink::create(['status' => StatusLink::STATUS_PENDING]);
        StatusLink::create(['status' => StatusLink::STATUS_OPEN]);
        StatusLink::create(['status' => StatusLink::STATUS_FILL]);
    }
}
