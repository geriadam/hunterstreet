<?php

use App\Models\Designer;
use Illuminate\Database\Seeder;

class DesignersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Designer::class, 100)->create();
    }
}
