<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRegisterDesignersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_register_designers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_register_id')
                ->references('id')
                ->on('user_registers');

            $table->foreignId('designer_id')
                ->references('id')
                ->on('designers');
                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_register_designers');
    }
}
