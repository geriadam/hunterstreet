<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Designer;
use Faker\Generator as Faker;

$factory->define(Designer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
