<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('pageTitle')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('admin/css/libs/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('admin/css/libs/select2/select2.min.css') }}">
</head>
<body>
    <div>
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <script src="{{ url('admin/js/jquery.min.js') }}"></script>
    <script src="{{ url('admin/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ url('admin/js/libs/select2/select2.full.min.js') }}"></script>
    <script src="{{ url('admin/js/libs/datepicker/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript">
        $('#designer').select2({
            placeholder: "Choose Designer...",
            minimumInputLength: 2,
            ajax: {
                url: '/designers/find',
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
        $('.datepicker').datepicker({
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            todayBtn: 'linked',
            language: 'id'
        });
    </script>
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
