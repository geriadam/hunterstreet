@extends('layouts.app')
@section('pageTitle', 'Register Expired')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Expired</div>
                <div class="card-body">
                    <h1>Link Expired</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
