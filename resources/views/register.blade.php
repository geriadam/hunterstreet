@extends('layouts.app')
@section('pageTitle', 'Register Event HUNTBAZAAR')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            @if ($link->code === null)
            <div id="app">
                <countdown starttime="{{ $startDate }}" endtime="{{ $endDate }}"></countdown>
            </div>
            <div class="card mt-5">
                <div class="card-header">Register Here</div>
                <div class="card-body">
                    @include('admin.includes._alert')
                    @include('admin.includes._message')
                    <form method="POST" action="{{ route('register.invite.store', ['link' => $link]) }}">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $link->email->email }}" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autocomplete="off" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birth_date" class="col-md-4 col-form-label text-md-right">Birth Date</label>
                            <div class="col-md-6">
                                <input id="birth_date" type="text" class="form-control datepicker" name="birth_date" value="{{ old('birth_date') }}" required autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="gender" class="col-md-4 text-md-right">Gender</label>

                            <div class="col-md-6">
                                <input type="radio" class="radio-inline" name="gender" value="m" @if(old('gender') === 'm') checked @endif> Male
                                <input type="radio" class="radio-inline" name="gender" value="f" @if(old('gender') === 'f') checked @endif> Female
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="designer" class="col-md-4 text-md-right">Favorite Designer</label>

                            <div class="col-md-6">
                                <select id="designer" name="designers[]" class="form-control" multiple></select>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @else
            <div class="card">
                <div class="card-header">Register On Event</div>
                <div class="card-body">
                    @include('admin.includes._alert')
                    @include('admin.includes._message')
                    <p>Your Code</p>
                    <h1>{{ $link->code }}</h1>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
