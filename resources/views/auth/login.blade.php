@extends('admin.layouts.login')
@section('login', 'login-box')
@section('content')
    <div class="card-body login-card-body">
        <h1 class="login-box-msg">{{ __('Login') }}</h1>
        <form method="POST" action="{{ route('login') }}" id="login_form">
            @csrf
            <div class="input-group mb-3">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                       aria-label="email" placeholder="Email"
                       name="email" value="{{ old('email') }}" required autofocus v-model="username">
                <div class="input-group-append">
                    <span class="fa fa-envelope input-group-text"></span>
                </div>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <div class="input-group mb-3">
                <input id="password" type="password"
                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required
                       aria-label="password" placeholder="Password"
                       v-model="password">
                <div class="input-group-append">
                    <span class="fa fa-lock input-group-text"></span>
                </div>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            <div class="row">
                <div class="col-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block btn-fla" id="login_button">
                        {{ __('Login') }}
                    </button>
                </div>
                <!-- /.col -->
            </div>
        </form>
    </div>
@endsection
