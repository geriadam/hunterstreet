<link rel="stylesheet" type="text/css" href="{{ url('admin/css/libs/font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('admin/css/libs/datatables/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('admin/css/libs/sweetalert/sweetalert.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('admin/css/libs/datepicker/datepicker3.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('admin/css/libs/select2/select2.min.css') }}">
