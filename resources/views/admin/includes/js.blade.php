<script src="{{ url('admin/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ url('admin/js/libs/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ url('admin/js/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('admin/js/libs/datatables/datatables.bootstrap4.js') }}"></script>
<script src="{{ url('admin/js/libs/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ url('admin/js/libs/datepicker/bootstrap-datepicker.id.js') }}"></script>
<script src="{{ url('admin/js/libs/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2-select').select2();
        $('.datepicker').datepicker({
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            todayBtn: 'linked',
            language: 'id'
        });
    });
    $(document).on('click', '.btn-delete', function(e) {
        e.preventDefault();
        var form = $("#form-delete");
        var url = $(this).data("url");
        swal({
            title: 'Confirm Delete',
            text: 'Are you sure to delete this item?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
                if (typeof url != 'undefined') {
                    form.attr("action", url);
                }
                form.submit();
            } else {
                swal('cancelled', 'Deletion Cancelled', 'error');
            }
        });
    });
</script>
