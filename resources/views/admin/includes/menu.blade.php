<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        @foreach(MenuHelper::getArrOfMenu() as $menu)
            @if($menu['visible'])
                @if($menu['header'])
                    <li class="nav-header">{{ $menu['label'] }}</li>
                @else
                    <li class="nav-item {{ isset($menu['items']) && $menu['items'] ? 'has-treeview' : '' }}">
                        <a 
                            href="{{ $menu['url'] != '#' ? route($menu['url']) : '#'  }}" 
                            class="nav-link {{ isset($menu['active']) && $menu['active'] ? "active" : "" }}"
                        >
                            <i class="fa fa-{{ $menu['icon'] }}"></i> 
                            <p>
                                {{ $menu['label'] }}
                                @if(isset($menu['items']) && $menu['items'])
                                    <i class="right fa fa-angle-left"></i>
                                @endif
                            </p>
                        </a>
                        @if(isset($menu['items']) && !empty($menu['items']))
                            <ul class="nav nav-treeview">
                                @foreach($menu['items'] as $secondMenu)
                                    @if(isset($secondMenu['visible']) && $secondMenu['visible'])
                                        <li class="nav-item">
                                            <a 
                                                href="{{ $secondMenu['url'] != '#' ? route($secondMenu['url']) : '#' }}" 
                                                class="nav-link {{ isset($secondMenu['active']) && $secondMenu['active'] ? "active" : "" }}"
                                            >
                                                <p>{{ $secondMenu['label'] }}</p>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endif
            @endif
        @endforeach
    </ul>
</nav>
@push('after_scripts')
    <script>
        // Set active state on menu element
        var full_url = "{{ Request::fullUrl() }}";
        var $navLinks = $("ul.nav li a");
        // First look for an exact match including the search string
        var $curentPageLink = $navLinks.filter(
            function() { return $(this).attr('href') === full_url; }
        );
        // If not found, look for the link that starts with the url
        if(!$curentPageLink.length > 0){
            $curentPageLink = $navLinks.filter(
                function() { return $(this).attr('href').startsWith(full_url) || full_url.startsWith($(this).attr('href')); }
            );
        }
        $curentPageLink.parents('li').addClass('menu-open');
    </script>
@endpush
