@extends('admin.layouts.template')
@section('pageTitle', 'Dashboard')
@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h5 class="m-0">Link Pending</h5>
                </div>
                <div class="card-body">
                    <h1>{{ $statusPending }}</h1>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h5 class="m-0">Link Opened</h5>
                </div>
                <div class="card-body">
                    <h1>{{ $statusOpen }}</h1>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h5 class="m-0">Link Filled</h5>
                </div>
                <div class="card-body">
                    <h1>{{ $statusFill }}</h1>
                </div>
            </div>
        </div>
    </div>
@endsection
