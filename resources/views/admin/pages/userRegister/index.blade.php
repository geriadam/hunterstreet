@extends('admin.layouts.template')
@section('pageTitle', 'User Register')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="user-register-table">
                            <thead>
                            <tr>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Birth Date</th>
                                <th>Gender</th>
                                <th>Designers</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after_scripts')
<script>
    $(function() {
        var oTable = $('#user-register-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin.userRegister.index') }}'
            },
            columns: [
                {data: 'email', name: 'email'},
                {data: 'name', name: 'name'},
                {data: 'birth_date', name: 'birth_date'},
                {data: 'gender_label', name: 'gender_label'},
                {data: 'designers_label', name: 'designers_label'},
            ],
            length: 2
        });
    });
</script>
@endpush

