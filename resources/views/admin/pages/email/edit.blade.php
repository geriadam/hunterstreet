@extends('admin.layouts.template')
@section('pageTitle', 'Email')
@section('content')
<div class="row">
    <div class="col-lg-12">
        {{ Form::open(['route' => ['admin.email.update', $email->id], 'method' => 'PUT']) }}
        <div class="card">
            <div class="card-body">
                @include('admin.includes._message')
                <div class="form-group">
                    <label>Email*</label>
                    {!! Form::text('email', $email->email, ["class" => "form-control"]) !!}
                </div>
            </div>
            <div class="card-footer">
                <a href="{{ url()->previous() }}">
                    <button type="button" class="btn btn-primary">
                        <i class="fa fa-arrow-left"></i> Back
                    </button>
                </a>
                <button type="submit" class="btn btn-success">
                    <i class="fa fa-save"></i> Update
                </button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
