@extends('admin.layouts.template')
@section('pageTitle', 'Email')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('admin.includes._alert')
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('admin.email.create') }}">
                        <button class="btn btn-success">
                            <i class="fa fa-plus"></i> Create
                        </button>
                    </a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="email-table">
                            <thead>
                            <tr>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after_scripts')
<script>
    $(function() {
        var oTable = $('#email-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin.email.index') }}'
            },
            columns: [
                {data: 'email', name: 'email'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            length: 2
        });
    });
</script>
@endpush

