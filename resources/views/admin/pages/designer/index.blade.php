@extends('admin.layouts.template')
@section('pageTitle', 'Designer')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            @include('admin.includes._alert')
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('admin.designer.create') }}">
                        <button class="btn btn-success">
                            <i class="fa fa-plus"></i> Create
                        </button>
                    </a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="designer-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after_scripts')
<script>
    $(function() {
        var oTable = $('#designer-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin.designer.index') }}'
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            length: 2
        });
    });
</script>
@endpush

