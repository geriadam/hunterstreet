@extends('admin.layouts.template')
@section('pageTitle', 'Link')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="link-table">
                            <thead>
                            <tr>
                                <th>Status</th>
                                <th>Email</th>
                                <th>URL</th>
                                <th>Code</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after_scripts')
<script>
    $(function() {
        var oTable = $('#link-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('admin.link.index') }}'
            },
            columns: [
                {data: 'status', name: 'status'},
                {data: 'email', name: 'email'},
                {data: 'url', name: 'url'},
                {data: 'code', name: 'code'},
            ],
            length: 2
        });
    });
</script>
@endpush

