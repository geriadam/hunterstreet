<div role="group" aria-label="Record actions" class="btn-group">
    @if(isset($editUrl))
        <a
            href="{!! $editUrl !!}"
            class="btn btn-success btn-sm"
            data-toggle="tooltip"
            title="Edit"
        >
            <i class="fa fa-pencil"></i>
        </a>
    @endif
    @if(isset($deleteUrl))
        <a
            data-url="{{ $deleteUrl }}"
            class="btn btn-danger btn-sm btn-delete"
            data-toggle="tooltip"
            id="btn-delete-{{ $row->id }}"
            style="color: #fff"
        >
            <i class="fa fa-trash"></i>
        </a>
    @endif
</div>
