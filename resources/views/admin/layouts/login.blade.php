<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ request()->route()->getName() == 'login' ? 'Login' : 'Register'  }} :: Admin | Hunterstreet </title>
    @stack('before_styles')
    <link rel="stylesheet" type="text/css" href="{{ url('css/plugin/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ url('admin/css/adminlte.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

    @stack('after_styles')
</head>
<body class="hold-transition login-page">
<div id="quick_login">
    <div class="container @yield('login') mb-0">
        <div class="row">
            <div class="col-12">
                <div class="login-logo">
                    <a href="{{ url('/') }}">
                        <img
                            src="{{ asset('uploads/logo-kkp.png') }}"
                            alt=""
                            class="img-fluid"
                            style="max-width: 150px"
                        >
                    </a>
                </div>
                <div class="card">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ url('admin/js/jquery.min.js') }}"></script>
@stack('before_scripts')
<script src="{{ url('admin/js/adminlte.min.js') }}"></script>
@stack('after_scripts')
</body>
</html>
