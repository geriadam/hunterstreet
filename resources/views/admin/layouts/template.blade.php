<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-param" content="_token"/>

    <title>@yield('pageTitle') :: Admin | Hunterstreet </title>
    @stack('before_styles')
    {{--Theme styles--}}
    <link rel="stylesheet" href="{{ url('admin/css/adminlte.min.css') }}">
    @include('admin.includes.head')
    @stack('after_styles')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            @auth
                <li class="nav-item mr-1">
                    <a href="#" class="btn btn-primary nav-link" style="color: #fff">
                        <i class="fa fa-user"></i> {{ auth()->user()->email }}
                    </a>
                </li>
            @endauth
            <li class="nav-item">
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button class="nav-link btn btn-danger" type="submit" style="color: #fff">
                        <i class="fa fa-sign-out"></i> {{ __('Logout') }}
                    </button>
                </form>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{ url('/') }}" class="brand-link">
            <span class="brand-text font-weight-light">Hunterstreet</span>
        </a>
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="info">
                    <a href="#" class="d-block">Hai, {{ auth()->check() ? auth()->user()->name : '' }}!</a>
                </div>
            </div>
            <!-- Sidebar Menu -->
        @include('admin.includes.menu')
        <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">@yield('pageTitle')</h1>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <footer class="main-footer d-print-none">
        <!-- Default to the left -->
        <strong>Copyright {{ date('Y') }}</strong>
    </footer>
    {!! Form::open(['id' => 'form-delete', 'method' => 'DELETE']) !!}
    {!! Form::close() !!}
</div>
<!-- ./wrapper -->
<!-- jQuery -->
<script src="{{ url('admin/js/jquery.min.js') }}"></script>
@stack('before_scripts')
<!-- REQUIRED SCRIPTS -->
<script src="{{ url('admin/js/adminlte.min.js') }}"></script>
@include('admin.includes.js')
@stack('after_scripts')
</body>
</html>
